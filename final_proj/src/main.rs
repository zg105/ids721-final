use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use actix_web_prom::PrometheusMetricsBuilder;
use prometheus::{opts, IntCounterVec};
use std::env;
use std::io::Write;
use std::path::PathBuf;
use std::convert::Infallible;
use llm::{self, InferenceFeedback, InferenceRequest, InferenceResponse, ModelArchitecture, TokenizerSource};

async fn generate_response(prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    println!("{}", env::current_dir().unwrap().display());
    let model_file = PathBuf::from("src/dolly-v2-3b-q4_0.bin");
    let model = llm::load_dynamic(
        Some(ModelArchitecture::GptNeoX),
        &model_file,
        TokenizerSource::Embedded,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;
    let mut session = model.start_session(Default::default());
    let mut response_text = String::new();

    let inference_result = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &InferenceRequest {
            prompt: (&prompt).into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(12),
        },
        &mut Default::default(),
        |response| match response {
            InferenceResponse::PromptToken(token) | InferenceResponse::InferredToken(token) => {
                print!("{token}");
                std::io::stdout().flush().unwrap();
                response_text.push_str(&token);
                Ok(InferenceFeedback::Continue)
            }
            _ => Ok(InferenceFeedback::Continue),
        },
    );

    inference_result.map(|_| response_text).map_err(|e| Box::new(e) as _)
}

async fn index(correct_counter: web::Data<IntCounterVec>, query: web::Query<std::collections::HashMap<String, String>>) -> impl Responder {
    let default_query = "How are you today?".to_string();
    let user_query = query.get("query").unwrap_or(&default_query);
    match generate_response(user_query.to_owned()).await {
        Ok(response) => HttpResponse::Ok().content_type("text/plain").body(response),
        Err(e) => HttpResponse::InternalServerError().body(format!("Error processing request: {:?}", e)),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let prometheus = PrometheusMetricsBuilder::new("api")
        . endpoint("/metrics")
        .build()
        .unwrap();
    let correct_total_opts = opts!("http_requests_correct_total", "Total number of correct HTTP requests.").namespace("api");
    let correct_total = IntCounterVec::new(correct_total_opts, &["endpoint"]).unwrap();
    prometheus.registry.register(Box::new(correct_total.clone())).unwrap();
    HttpServer::new(move || {
        App::new()
        .wrap(prometheus.clone()).app_data(web::Data::new(correct_total.clone()))
        .route("/asksmt", web::get().to(index))
        .route("/", web::get().to(|| async { "Hello, World!" }))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
