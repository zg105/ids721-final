# IDS721 Final Project: Scalable Server with LLM Integration


Welcome to the repository of Group 21's project for the IDS721 course. In this project, we have developed a scalable server infrastructure using Rust, which is integrated with an open-source large language model (LLM) from Huggingface. Our server setup includes robust monitoring, containerization, and deployment strategies, as well as a CI/CD pipeline to streamline development and deployment processes.

## Project Overview

The main objective of our project was to create a reliable and scalable web service that can handle the inference workload of a large language model while maintaining high availability and resilience. We chose Rust for its performance and safety features, which are critical for building efficient and secure web services.

## Team Members

- Zhichen Guo (zg105)
- Yunjia Liu (yl794)
- Junhan Xu (jx139)

## Key Features

### Cargo Web Service

We initiated our project by creating a new cargo project. The following steps were executed:

1. **Initialization**: Start a new cargo project by running `cargo new final-proj`.
2. **Dependencies**: Add necessary dependencies to `Cargo.toml` to ensure our server can handle web requests, interact with the LLM, and perform logging and monitoring effectively.

```toml
[dependencies]
actix-web = "4.5.1"
actix-web-prom = "0.8.0"
lambda_http = "0.11.1"
tokio = { version = "1", features = ["macros", "rt-multi-thread"] }
tracing = "0.1.27"
log = "0.4.14"
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
openssl = { version = "0.10.35", features = ["vendored"] }
serde = {version = "1.0", features = ["derive"] }
serde_json = "1.0"
rand = "0.8.5"
prometheus = "0.13"
```

3. **LLM Integration**: Integrated a LLM model from Huggingface.co [here](https://huggingface.co/rustformers/dolly-v2-ggml/tree/main) to process incoming text queries and generate responses based on the model's output.
4. **HTTP Server Setup**: Utilized actix-web to handle HTTP requests. We parse query from url and generate response according to LLM model.
![](pic/pic1.png) \
5. We implemented plenty of tests using `cargo test`, including different query format, and empty query. For empty query, we used a default value. 
```
async fn index(query: web::Query<std::collections::HashMap<String, String>>) -> impl Responder {
    let default_query = "How are you today?".to_string();
    let user_query = query.get("query").unwrap_or(&default_query);
    match generate_response(user_query.to_owned()).await {
        Ok(response) => HttpResponse::Ok().content_type("text/plain").body(response),
        Err(e) => HttpResponse::InternalServerError().body(format!("Error processing request: {:?}", e)),
    }
}
```

## Containerize
1. Create `Dockerfile` and build docker image by `sudo docker build -t <image-name> .`
2. Push docker image to aws ECR 
![](pic/pic2.png)

## Deploy to aws EKS
1. Create a new EKS cluster
2. Create `final-deployment.yaml` for deployment. Set replicas to 3 to enhance availability. 
```
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: final-deployment
  name: final-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: final-deployment
  strategy: {}
  template:
    metadata:
      annotations:
        instrumentation.opentelemetry.io/inject-java: "true"
      creationTimestamp: null
      labels:
        app: final-deployment
    spec:
      containers:
      - name: ids721-final
        image: 637423589491.dkr.ecr.us-east-1.amazonaws.com/ids721-final:latest
        ports:
        - containerPort: 80

```
3. Create `final-service.yaml` for service, expose port 8080 for external web, match the label of dinal-deployment.
```
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: final-service
  name: final-service
spec:
  ports:
  - port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: final-deployment
status:
  loadBalancer: {}

```
4. Deploy using `kubectl apply -f final-deployment` and `kubectl apply -f final-service`. View the status of deployment and see 3 pods are running.
![](pic/pic3.png)

## CI/CD pipeline

We incluede two stages: build and deploy.
1. build: after changes in main branch, we rebuild the docker image and push it to ECR.
```
build_image:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - apk add --no-cache docker py-pip python3-dev libffi-dev openssl-dev gcc libc-dev rust cargo make 
    - python3 -m venv venv
    - source venv/bin/activate
    - pip install --upgrade pip
    - pip install awscli
    - docker login -u AWS -p $(aws ecr get-login-password --region us-east-1) "$aws_account_id.dkr.ecr.$region.amazonaws.com"
    - docker build -t "$aws_account_id.dkr.ecr.$region.amazonaws.com/ids721-final:latest" .
    - docker push "$aws_account_id.dkr.ecr.$region.amazonaws.com/ids721-final:latest"
  only:
    - main
```
2. deploy: after building and pushing docker image, we apply the new k8s config and redeploy deployment and service.
```
deploy:
  stage: deploy
  image: debian:latest
  before_script:
    - apt-get update && apt-get install -y curl tar awscli
    - curl -O https://s3.us-west-2.amazonaws.com/amazon-eks/1.29.0/2024-01-04/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
    - mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$HOME/bin:$PATH
    - export ARCH=amd64
    - export PLATFORM=$(uname -s)_$ARCH
    - curl -sLO "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_$PLATFORM.tar.gz"
    - tar -xzf eksctl_$PLATFORM.tar.gz -C $HOME/bin && rm eksctl_$PLATFORM.tar.gz
  script:
    - aws eks update-kubeconfig --region $region --name ids721-final-cluster
    - kubectl apply -f final_proj/final-deployment.yaml
    - kubectl apply -f final_proj/final-service.yaml
  only:
    - main
```
## Monitoring
1. We use prometheus to test the core metrics of our service:
```
    let correct_total_opts = opts!("http_requests_correct_total", "Total number of correct HTTP requests.").namespace("api");
    let correct_total = IntCounterVec::new(correct_total_opts, &["endpoint"]).unwrap();
    prometheus.registry.register(Box::new(correct_total.clone())).unwrap();
```
![](pic/pic5.png)
2. Use AWS control panel to monitor health and usage for working nodes:
![](pic/pic4.png)

### Demo and Further Documentation

For a complete demonstration of the functionalities and detailed setup, please refer to our [demo video](https://youtu.be/SSZCZjId_Qo).